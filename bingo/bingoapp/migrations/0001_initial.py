# Generated by Django 2.1.1 on 2018-10-01 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bingocell',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tagline', models.CharField(max_length=200)),
                ('date_added', models.DateTimeField(verbose_name='date published')),
            ],
        ),
    ]
