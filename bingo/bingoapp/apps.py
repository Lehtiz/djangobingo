from django.apps import AppConfig


class BingoappConfig(AppConfig):
    name = 'bingoapp'
