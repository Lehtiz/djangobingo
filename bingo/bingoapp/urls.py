from django.urls import path, re_path

from . import views

urlpatterns = [
    # /bingo/
    path('', views.index, name='index'),

    # /bingo/<genreName>/grid
    re_path(r'(?P<genreName>\w+)/grid/', views.grid, name='grid'),

    # /bingo/<genreName>/view
    re_path(r'(?P<genreName>\w+)/view/', views.genreview, name='genreview'),
]