from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import BingoCell, BingoGenre

def index(request):
    genrelist = BingoGenre.objects.all()
    template = loader.get_template('bingoapp/index.html')
    context = {
        'genrelist': genrelist,
    }
    return HttpResponse(template.render(context, request))

def grid(request, genreName):
    template = loader.get_template('bingoapp/grid.html')

    genreId = BingoGenre.objects.get(name=genreName)
    #get data from database, filter by genre and shuffle
    query_results = BingoCell.objects.filter(genre=genreId).order_by('?')

    context = {
        'context': query_results,
    }
    return HttpResponse(template.render(context, request))

def genreview(request, genreName):
    template = loader.get_template('bingoapp/genreview.html')
    
    genreId = BingoGenre.objects.get(name=genreName)
    query_results = BingoCell.objects.filter(genre=genreId)
    context = {
        'context': query_results,
    }
    return HttpResponse(template.render(context, request))