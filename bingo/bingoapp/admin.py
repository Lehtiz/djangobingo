from django.contrib import admin

# Register your models here.
from .models import BingoCell, BingoGenre


class BingoCellAdmin(admin.ModelAdmin):
    list_display = ('id', 'genre', 'tagline', 'was_added_recently')

class BingoGenreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(BingoCell, BingoCellAdmin)
admin.site.register(BingoGenre, BingoGenreAdmin)
