import datetime

from django.db import models
from django.utils import timezone



class BingoGenre(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)

    # default call to print out name attribute for ex. admin page
    def __str__(self):
        return self.name

class BingoCell(models.Model):
    id = models.AutoField(primary_key=True)
    genre = models.ForeignKey(BingoGenre,default=0, on_delete=models.CASCADE)
    tagline = models.CharField(max_length=200)
    date_added = models.DateTimeField('date published')

    def __str__(self):
        return self.tagline
        
    def was_added_recently(self):
        #return self.date_added >= timezone.now() - datetime.timedelta(days=1)
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.date_added <= now
        was_published_recently.admin_order_field = 'date_added'
        was_published_recently.boolean = True
        was_published_recently.short_description = 'Published recently?'


##vote/rating system ?